# Amr-Ismail Express Vue App

This is a test app called TabTracker 

## Description

This app is using expressjs + vue + quasar framework

## Getting Started

### Installing

* Install required dependencies using the command "npm install" inside the server directory
* Install required dependencies using the command "npm install" inside the client directory

### Executing program

* Create a .env file and add the required fields. 
* Run the following inside the server directory: npm run start
* Run the following inside the client directory: npm run serve


### Database & Seeding Data

* Run the following commands to init your database

    CREATE DATABASE `tab_tracker` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;

    CREATE TABLE `songs` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
    `artist` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
    `genre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
    `album` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
    `albumImage` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
    `youtubeId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
    `lyrics` TEXT(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `tab` TEXT(1000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

    CREATE TABLE `users` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `email` varchar(255) DEFAULT NULL,
    `password` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `email_UNIQUE` (`email`)
    ) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

* Run the following inside the server directory to seed songs: npm run seed

## Authors

* Amr Ismail : Main Developer
* Anne Marinas  : Mentor