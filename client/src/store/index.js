import { createStore } from "vuex";
export default createStore({
  strict:true,
  state: {
    token: null || localStorage.getItem('token'),
    user: null || localStorage.getItem('user'),
    songs: []
  },
  getters: {
    isUserLoggedIn: state => !!state.token,
    songs: state => state.songs,
    getUser: state => state.user
  },
  mutations: {
    setToken(state, token){
      state.token = token 
    },
    setUser(state, user){
      state.user = user 
    },
    setSongs(state, songs){
      state.songs = songs 
    }
  },
  actions: {
    setToken({commit}, token){
      commit('setToken', token)
      if(token){
        localStorage.setItem( 'token', token );
      }else{
        localStorage.removeItem('token')
      }
    },
    setUser({commit}, user){
      commit('setUser', user)
      if(user){
        localStorage.setItem( 'user', user.id );
      }else{
        localStorage.removeItem('user')
      }
    },
    setSongs({commit}, songs){
      commit('setSongs', songs)
    }
  }
});