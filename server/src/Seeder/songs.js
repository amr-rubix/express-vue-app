const pool = require('../db/pool')
const squel = require('squel')
const escape = require('sql-escape')

const songs = [
  {
    title: 'Someone Like You ',
    artist: 'Adele',
    genre: 'Romance',
    album: '21',
    albumImage: 'https://upload.wikimedia.org/wikipedia/en/7/7a/Adele_-_Someone_Like_You.png',
    youtubeId: 'hLQl3WQQoQ0',
    lyrics: `I heard that you're settled down
    That you found a girl and you're married now
    I heard that your dreams came true
    Guess she gave you things, I didn't give to you
    Old friend, why are you so shy?
    Ain't like you to hold back or hide from the light
    I hate to turn up out of the blue, uninvited
    But I couldn't stay away, I couldn't fight it
    I had hoped you'd see my face
    And that you'd be reminded that for me, it isn't over
    Never mind, I'll find someone like you
    I wish nothing but the best for you, too
    &quot;Don't forget me, &quot; I beg
    I remember you said
    &quot;Sometimes it lasts in love, but sometimes it hurts instead&quot;
    &quot;Sometimes it lasts in love, but sometimes it hurts instead&quot;
    You know how the time flies
    Only yesterday was the time of our lives
    We were born and raised in a summer haze
    Bound by the surprise of our glory days
    I hate to turn up out of the blue, uninvited
    But I couldn't stay away, I couldn't fight it
    I had hoped you'd see my face
    And that you'd be reminded that for me, it isn't over
    Never mind, I'll find someone like you
    I wish nothing but the best for you, too
    &quot;Don't forget me, &quot; I begged
    I remember you said
    &quot;Sometimes it lasts in love, but sometimes it hurts instead&quot;
    Nothing compares, no worries or cares
    Regrets and mistakes, they're memories made
    Who would have known how bittersweet this would taste?
    Never mind, I'll find someone like you
    I wish nothing but the best for you
    &quot;Don't forget me, &quot; I beg
    I remember you said
    &quot;Sometimes it lasts in love, but sometimes it hurts instead&quot;
    Never mind, I'll find someone like you
    I wish nothing but the best for you, too
    &quot;Don't forget me, &quot; I begged
    I remember you said
    &quot;Sometimes it lasts in love, but sometimes it hurts instead&quot;
    &quot;Sometimes it lasts in love, but sometimes it hurts instead&quot;`,
    tab: `* Adele - Someone like you *

    (simplified)
    
    
    G
    i heard
               D
    that you'll settle down
              Em
    that you've found a girl
            C
    and you're married now
    G
    i heard
               D
    that your dreams came true
              Em
    guess she gave you things
             C
    i didn't give to you
    
       G
    old friend
                D
    why are you so shy
                  Em
    it ain't like you to hold back
       C
    or hide from the light`
  },
  {
    title: 'Bad',
    artist: 'Michael Jackson',
    genre: 'Pop',
    album: 'Bad',
    albumImage: 'https://i.pinimg.com/originals/ba/02/d3/ba02d352a3fbd0608b5033eb0324042c.jpg',
    youtubeId: 'dsUXAEzaC3Q',
    lyrics: `Your Butt Is Mine
    Gonna Take You Right
    Just Show Your Face
    In Broad Daylight
    I'm Telling You
    On How I Feel
    Gonna Hurt Your Mind
    Don't Shoot To Kill
    Come On, Come On,
    Lay It On Me All Right
    I'm Giving You
    On Count Of Three
    To Show Your Stuff
    Or Let It Be
    I'm Telling You
    Just Watch Your Mouth
    I Know Your Game
    What You're About
    Well They Say The Sky's
    The Limit
    And To Me That's Really True
    But My Friend You Have
    Seen Nothing
    Just Wait 'Til I Get Through
    Because I'm Bad, I'm Bad-
    Come On
    (Bad Bad-Really, Really Bad)
    You Know I'm Bad, I'm Bad-
    You Know It
    (Bad Bad-Really, Really Bad)
    You Know I'm Bad, I'm Bad-
    Come On, You Know
    (Bad Bad-Really, Really Bad)
    And The Whole World Has To
    Answer Right Now
    Just To Tell You Once Again,
    Who's Bad . . .
    The Word Is Out
    You're Doin' Wrong
    Gonna Lock You Up
    Before Too Long,
    Your Lyin' Eyes
    Gonna Take You Right
    So Listen Up
    Don't Make A Fight,
    Your Talk Is Cheap
    You're Not A Man
    You're Throwin' Stones
    To Hide Your Hands
    But They Say The Sky's
    The Limit
    And To Me That's Really True
    And My Friends You Have
    Seen Nothin'
    Just Wait 'Til I Get Through
    Because I'm Bad, I'm Bad-
    Come On
    (Bad Bad-Really, Really Bad)
    You Know I'm Bad, I'm Bad
    You Know It
    (Bad Bad-Really, Really Bad)
    You Know I'm Bad, I'm Bad
    You Know It, You Know
    (Bad Bad-Really, Really Bad)
    And The Whole World Has To
    Answer Right Now
    (And The Whole World Has To
    Answer Right Now)
    Just To Tell You Once Again,
    (Just To Tell You Once Again)
    Who's Bad . . .
    We Can Change The World
    Tomorrow
    This Could Be A Better Place
    If You Don't Like What I'm
    Sayin'
    Then Won't You Slap My
    Face . . .
    Because I'm Bad, I'm Bad
    Come On
    (Bad Bad-Really, Really Bad)
    You Know I'm Bad, I'm Bad
    You Know It
    (Bad Bad-Really, Really Bad)
    You Know I'm Bad, I'm Bad
    You Know It, You Know
    (Bad Bad-Really, Really Bad)
    Woo! Woo! Woo!
    (And The Whole World Has
    To Answer Right Now
    Just To Tell You Once
    Again . . .)
    You Know I'm Bad, I'm Bad
    Come On
    (Bad Bad-Really, Really Bad)
    You Know I'm Bad, I'm Bad
    You Know It-You Know It
    (Bad Bad-Really, Really Bad)
    You Know, You Know, You
    Know, Come On
    (Bad Bad-Really, Really Bad)
    And The Whole World Has To
    Answer Right Now
    (And The Whole World Has To
    Answer Right Now)
    Just To Tell You
    (Just To Tell You Once Again)
    You Know I'm Smooth, I'm
    Bad, You Know It
    (Bad Bad-Really, Really Bad)
    You Know I'm Bad, I'm
    Bad Baby
    (Bad Bad-Really, Really Bad)
    You Know, You Know, You
    Know It, Come On
    (Bad Bad-Really, Really Bad)
    And The Whole World Has To
    Answer Right Now
    (And The Whole World Has To
    Answer Right Now)
    Woo!
    (Just To Tell You Once Again)
    You Know I'm Bad, I'm Bad
    You Know It
    (Bad Bad-Really, Really Bad)
    You Know I'm Bad-You
    Know-Hoo!
    (Bad Bad-Really, Really Bad)
    You Know I'm Bad-I'm Bad-
    You Know It, You Know
    (Bad Bad-Really, Really Bad)
    And The Whole World Has To
    Answer Right Now
    (And The Whole World Has To
    Answer Right Now)
    Just To Tell You Once Again
    (Just To Tell You Once
    Again
    Who's Bad?`,
    tab: `* Adele - Someone like you *

    (simplified)
    
    
    G
    i heard
               D
    that you'll settle down
              Em
    that you've found a girl
            C
    and you're married now
    G
    i heard
               D
    that your dreams came true
              Em
    guess she gave you things
             C
    i didn't give to you
    
       G
    old friend
                D
    why are you so shy
                  Em
    it ain't like you to hold back
       C
    or hide from the light`
  }
]

for (let index = 0; index < songs.length; index++) {
  const song = songs[index]
  const escapedSong = {
    ...song,
    lyrics: escape(song.lyrics),
    tab: escape(song.tab)
  }

  const query = squel
    .insert()
    .into('songs')
    .setFieldsRows([escapedSong])
    .toString()
  pool.query(query, (err, result) => {
    if (err) {
      console.log('error here', err)
      return
    }
    console.log('Seeder Done')
  })
}
