const Song = require('../models/Song')
const chai = require('chai')

module.exports = {
  async index (req, res) {
    try {
      const songs = await Song.findAll(10)
      res.send(songs)
    } catch (error) {
      res.status('500').send({
        error: 'An error occured trying to fetch songs'
      })
    }
  },
  async show (req, res) {
    try {
      const song = await Song.get_song(req.params.songId)
      res.send(song)
    } catch (error) {
      console.log('error', error)
      res.status('500').send({
        error: 'An error occured trying to fetch song'
      })
    }
  },
  async post (req, res) {
    let song = req.body

    /**
     * using chai test for validating a user before creating a song
     */

    try {
      chai.assert.notEqual(song.userId, null)
    } catch (error) {
      return res.status('403').send({
        error: 'Sorry! You need to be logged in for creating a new song.'
      })
    }
    try {
      delete song.userId
      song = await Song.create(song)
      res.send(song)
    } catch (error) {
      console.log('error here', error)
      res.status('500').send({
        error: 'An error occured trying to create song'
      })
    }
  },
  async put (req, res) {
    try {
      const song = await Song.update(req.body, req.params.songId)
      res.send(song)
    } catch (error) {
      res.status('500').send({
        error: 'An error occured trying to update song'
      })
    }
  }
}
